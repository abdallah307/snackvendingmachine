package com.company;

public class SnackSlot {

    private int items[][];

    public SnackSlot() {
         this.items = new int[5][5];
         int count = 0;
         for(int i = 0;i < 5;i++) {
             for(int j = 0;j < 5;j++) {
                this.items[i][j] = count++;
             }
         }
    }

    public int chooseItem(int itemNumber) {
        int item = -1;
        for(int i = 0;i < 5;i++) {
            for(int j = 0;j < 5;j++) {
                if (itemNumber == (items[i][j])) {
                    item = items[i][j];
                }
            }
        }

        return item;
    }
}
