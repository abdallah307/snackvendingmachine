package com.company;

public class Calculator {
    public static double calculateAccumulatedMoneyAmount(double moneyAmount, double insertedMoneyAmount) {
        double result = moneyAmount + insertedMoneyAmount;
        return result;
    }

    public static double calculateChangeAmount(double moneyAmount, double itemPrice) {
        double result = moneyAmount - itemPrice;
        return result;
    }
}
