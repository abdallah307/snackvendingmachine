package com.company;

public class Keypad {
    private String enteredNumber;

    public void selectNumber(String number) {
        this.enteredNumber = number;
    }

    public int getEnteredNumber() {
        return Integer.parseInt(this.enteredNumber);
    }
}
