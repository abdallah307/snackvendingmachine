package com.company;

public class SnackVendingMachine extends VendingMachine {

    private MoneySlot coinSlot;
    private MoneySlot notesSlot;
    private CardSlot cardSlot;
    private Keypad keypad;

    private double moneyAmount;
    private Item selectedItem;

    public SnackVendingMachine() {
        this.coinSlot = new CoinSlot();
        this.notesSlot = new NotesSlot();
        this.cardSlot = new CardSlot();
        this.keypad = new Keypad();
        this.moneyAmount = 0.0;
        this.selectedItem = null;
    }

    public void resetMachine() {
        this.moneyAmount = 0.0;
        this.selectedItem = null;
    }

    @Override
    public void dispenseChange() {
        System.out.println("Take your change");
    }

    @Override
    public void enterItemId(int itemId) {
        this.selectedItem = Item.getSelectedItem(itemId);
        if (selectedItem != null) {
            VendingMachineDisplay.displaySnackInfo(this.selectedItem);
        }
        else {
            VendingMachineDisplay.displaySnackIsNotAvailable();
        }
    }

    @Override
    public void insertCoin(double amount) {
        this.insertMoney(coinSlot,amount);
    }

    @Override
    public void insertNote(double amount) {
        this.insertMoney(notesSlot, amount);
    }

    @Override
    public void insertCard(Card card) {
        if (cardSlot.isValid(card,selectedItem.getPrice())) {
            moneyAmount = card.getBalance() - selectedItem.getPrice();
            System.out.println(selectedItem.getPrice() + "$ has been deducted from your account");
            dispenseProduct();
            resetMachine();
        }
        else {
            VendingMachineDisplay.displayCardBalanceNotEnough();
        }
    }

    public void insertMoney(MoneySlot moneySlot, double amount) {
        if (moneySlot.isValid(amount)) {
            if (!isMoneyEnough(amount)) {
                this.moneyAmount += amount;
            }
            else {
                dispenseProduct();
                double change = Calculator.calculateChangeAmount(moneyAmount,selectedItem.getPrice());
                VendingMachineDisplay.displayChange(change);
                if (change > 0.0) {
                    dispenseChange();
                }
                resetMachine();
                return;
            }
            VendingMachineDisplay.displayAccumulatedMoneyAmount(moneyAmount);

            return;
        }
        else {
            System.out.println("The Entered Amount is not Valid!!");
        }

    }


    @Override
    public void dispenseProduct() {
        dispenseSnack();
    }

    public void dispenseSnack() {
        System.out.println("Take your item : " + selectedItem.getItemId());
    }

    public boolean isMoneyEnough(double insertedAmount) {
         double accumulatedAmount = Calculator.calculateAccumulatedMoneyAmount(moneyAmount,insertedAmount);
         if (accumulatedAmount >= selectedItem.getPrice()) {
             moneyAmount += insertedAmount;
             return true;
         }
         return false;
    }


}