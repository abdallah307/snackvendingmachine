package com.company;

public interface MoneySlot {
    public boolean isValid(double amount);
}
