package com.company;

public abstract class VendingMachine {
     CoinSlot coinSlot;
     NotesSlot notesSlot;
     CardSlot cardSlot;
     Keypad keypad;
     Calculator calculator;

     public abstract void dispenseChange();
     public abstract void enterItemId(int itemId);
     public abstract void insertCoin(double amount);
     public abstract void insertNote(double amount);
     public abstract void insertCard(Card card);
     public abstract void dispenseProduct();

}
