package com.company;

public enum NoteType{
    TWENTY_DOLLAR(20.0),
    FIFTY_DOLLAR(50.0);

    private double price;

    NoteType(double price) {

        this.price = price;
    }



    public double getPrice() {
        return this.price;
    }

    public static boolean checkValidNote(double amount) {
        for (NoteType noteType : NoteType.values()) {
            if (noteType.getPrice() == amount) {
                return true;
            }
        }

        return false;
    }
}
