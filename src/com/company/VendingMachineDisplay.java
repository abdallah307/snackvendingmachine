package com.company;

public class VendingMachineDisplay {

    public static void displaySnackInfo(Item selectedProduct) {
        System.out.println("Selected item : " +selectedProduct.getItemId());
        System.out.println("item price : " +selectedProduct.getPrice() + "$");
    }

    public static void displaySnackIsNotAvailable() {
        System.out.println("The selected snack is not available!!");
    }

    public static void displayCardBalanceNotEnough() {
        System.out.println("Sorry, Your card balance is not enough!!");
    }



    public static void displayAccumulatedMoneyAmount(double moneyAmount) {
        System.out.println("Accumulated Money Amount : " + moneyAmount + "$");
    }
    public static void displayChange(double change) {
       System.out.println("remaining change is : " + ((float)(change)) + "$");
    }

}
