package com.company;

public enum CoinType {
    TEN_CENT(0.1),
    TWENTY_CENT(0.2),
    FIFTY_CENT(0.5),
    ONE_DOLLAR(1.0);

    private double price;

    CoinType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return this.price;
    }

    public static boolean checkValidCoin(double amount) {
        for (CoinType coinType : CoinType.values()) {
            if (coinType.getPrice() == amount) {
                return true;
            }
        }

        return false;
    }
}
