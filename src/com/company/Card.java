package com.company;

public class Card {
    private double balance;

    public Card(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return this.balance;
    }
}
