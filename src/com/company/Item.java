package com.company;

public enum Item {

    LaysCheese(1,1.0),BayChips(2, 20.0),Ruffles(3,0.4),Lotus(4,4.0),Fritos(5,0.9),
    Cheetos(6,0.1),karnoush(7,10.0),Gardens(8,6.0),Twix(9,1.0),Mars(10,2.0),
    Cadbory(11,0.7),Crazy(12,0.2),Bodo(13,7),Mms(14,9),Wholee(15,0.3),
    MrChips(16,70.0),Askimo(17,0.1),Orbit(18,0.3),Apella(19,50.0),Popcat(20,1.0),
    Kitkat(21,3.0),Grandmas(22,0.8),Wafer(23,0.7),Doritos(24,0.5),Oreo(25,0.8);


    private int itemId;
    private double price;

    Item(int itemId, double price) {
        this.itemId = itemId;
        this.price = price;
    }

    public int getItemId() {
        return itemId;
    }


    public double getPrice() {
        return price;
    }

    public static Item getSelectedItem(int itemId) {
        Item selectedItem = null;
        for (Item item: Item.values()) {
            if (item.getItemId() == itemId) {
               selectedItem = item;
            }
        }

        return selectedItem;
    }
}
