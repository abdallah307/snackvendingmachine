package com.company;

public class NotesSlot implements MoneySlot{


    public boolean isValid(double amount) {
        if (NoteType.checkValidNote(amount)) {
            System.out.println("Inserted a Note");
            return true;
        }

        return false;
    }
}
